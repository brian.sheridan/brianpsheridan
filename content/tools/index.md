---
title: "Tools"
menu: "main"
---

I use both a Mac and PC for doing my development. I mostly use C# and AWS for hosting my code.  I have also started to learn python.

## Local Develement Tools

* Visual Studio (for Mac and Windows)
* Visual Studio Code
* Azure Data Studio 
* [PyCharm](https://www.jetbrains.com/pycharm/)
* [AWS Local](https://github.com/localstack/localstack)


## AWS Development Tools
* [AWS CDK](https://aws.amazon.com/cdk/)

## Online Tools
* [Github](https://github.com/two4suited/)
* [Azure Dev Ops](https://dev.azure.com/)

## Documentation 
* [Draw.IO](https://www.draw.io/)
